package com.drohobytskyy.XML.parser.sax;


import com.drohobytskyy.XML.Model.Gemstone;
import com.drohobytskyy.XML.Model.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Gemstone> gemstoneList = new ArrayList<>();
    private Gemstone gemstone = null;
    private VisualParameters visualParams = null;

    private boolean bName = false;
    private boolean bPreciousness = false;
    private boolean bOrigin = false;
    private boolean bValue = false;
    private boolean bVisualParams = false;
    private boolean bColour = false;
    private boolean bWayOfCutting = false;
    private boolean bTransparency = false;

    public List<Gemstone> getGemstoneList(){
        return this.gemstoneList;
    }

    public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("gemstone")){
            String gemstoneN = attributes.getValue("gemstoneNo");
            gemstone = new Gemstone();
            gemstone.setGemstoneNo(Integer.parseInt(gemstoneN));
        }
        else if (qName.equalsIgnoreCase("name"))            {bName = true;}
        else if (qName.equalsIgnoreCase("preciousness"))    {bPreciousness = true;}
        else if (qName.equalsIgnoreCase("origin"))          {bOrigin = true;}
        else if (qName.equalsIgnoreCase("value"))           {bValue = true;}
        else if (qName.equalsIgnoreCase("visualParameters")){bVisualParams = true;}
        else if (qName.equalsIgnoreCase("colour"))          {bColour = true;}
        else if (qName.equalsIgnoreCase("wayOfCutting"))    {bWayOfCutting = true;}
        else if (qName.equalsIgnoreCase("transparency"))    {bTransparency = true;}
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("gemstone")){
            gemstone.setVisualParams(visualParams);
            gemstoneList.add(gemstone);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (bName){
            gemstone.setName(new String(ch, start, length));
            bName = false;
        }
        else if (bPreciousness){
            gemstone.setPreciousness(Boolean.parseBoolean(new String(ch, start, length)));
            bPreciousness = false;
        }
        else if (bOrigin){
            gemstone.setOrigin(new String(ch, start, length));
            bOrigin = false;
        }
        else if (bValue){
            gemstone.setValue(Integer.parseInt(new String(ch, start, length)));
            bValue = false;
        }
        else if (bVisualParams){
            visualParams = new VisualParameters();
            bVisualParams = false;
        }
        else if (bColour){
            visualParams.setColour(new String(ch, start, length));
            bColour = false;
        }
        else if (bWayOfCutting){
            visualParams.setWayOfCutting(Integer.parseInt(new String(ch, start, length)));
            bWayOfCutting = false;
        }
        else if (bTransparency){
            visualParams.setTransparency(Integer.parseInt(new String(ch, start, length)));
            bTransparency = false;
        }
    }
}

