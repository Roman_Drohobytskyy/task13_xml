package com.drohobytskyy.XML.parser;


import com.drohobytskyy.XML.parser.dom.DOMParserUser;
import com.drohobytskyy.XML.parser.sax.SAXParserUser;
import com.drohobytskyy.XML.parser.stax.StAXReader;
import com.drohobytskyy.XML.Model.GemstoneComparator;
import com.drohobytskyy.XML.Model.Gemstone;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class Parser {

  public static void main(String[] args) {
    File xml = new File("src\\main\\resources\\xml\\gemstonesXML.xml");
    File xsd = new File("src\\main\\resources\\xml\\gemstonesXSD.xsd");

    if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            SAX
      printList(SAXParserUser.parseGemstones(xml, xsd), "SAX");

//            StAX
      printList(StAXReader.parseGestones(xml, xsd), "StAX");

//            DOM
      printList(DOMParserUser.getGemstoneList(xml, xsd), "DOM");
    }
  }

  private static boolean checkIfXSD(File xsd) {
    return xsd.isFile() && FilenameUtils.getExtension(xsd.getName()).equals("xsd");
  }

  private static boolean checkIfXML(File xml) {
    return xml.isFile() && FilenameUtils.getExtension(xml.getName()).equals("xml");
  }

  private static void printList(List<Gemstone> gemstones, String parserName) {
    Collections.sort(gemstones, new GemstoneComparator());
    System.out.println(parserName);
    for (Gemstone gemstone : gemstones) {
      System.out.println(gemstone);
    }
  }

}
