package com.drohobytskyy.XML.parser.dom;

import com.drohobytskyy.XML.Model.Gemstone;
import com.drohobytskyy.XML.Model.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<Gemstone> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Gemstone> gemstones = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("gemstone");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Gemstone gemstone = new Gemstone();
            VisualParameters visualParams;
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;

                gemstone.setGemstoneNo(Integer.parseInt(element.getAttribute("gemstoneNo")));
                gemstone.setName(element.getElementsByTagName("name").item(0).getTextContent());
                gemstone.setPreciousness(Boolean.parseBoolean(element.getElementsByTagName("preciousness")
                        .item(0).getTextContent()));
                gemstone.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
                gemstone.setValue(Double.parseDouble(element.getElementsByTagName("value")
                        .item(0).getTextContent()));
                visualParams = getVisualParameters(element.getElementsByTagName("visualParameters"));

                gemstone.setVisualParams(visualParams);
                gemstones.add(gemstone);
            }
        }
        return gemstones;
    }

    private VisualParameters getVisualParameters(NodeList nodes){
        VisualParameters visualParams = new VisualParameters();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            visualParams.setColour(element.getElementsByTagName("colour").item(0).getTextContent());
            visualParams.setWayOfCutting(Integer.parseInt(element.getElementsByTagName("wayOfCutting")
                    .item(0).getTextContent()));
            visualParams.setTransparency(Integer.parseInt(element.getElementsByTagName("transparency")
                    .item(0).getTextContent()));
        }
        return visualParams;
    }

}
