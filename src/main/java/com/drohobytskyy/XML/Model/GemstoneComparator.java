package com.drohobytskyy.XML.Model;

import java.util.Comparator;

public class GemstoneComparator implements Comparator<Gemstone> {

    @Override
    public int compare(Gemstone o1, Gemstone o2) {
        return Double.compare(o1.getValue(), o2.getValue());
    }
}
